import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { UserService } from './user.service';
import { Router } from '@angular/router';
import { Carrier, DeleteCarrierResponse, GetCarriersResponse, PostCarrierResponse } from './admin/carrier';
import { CarrierService } from './carrier.service';

export interface User {
  email: string;
  password: string;
}

export interface LoginResponse {
  token: string;
}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'X-Frame-Options': 'SAMEORIGIN',
    'X-XSS-Protection': '1; mode=block',
    'X-Content-Type-Options': 'nosniff',
    traza: 'swagger-traza',
    aplicacionorigen: 'swagger',
    Authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJMb2dpbiI6eyJVc2VySWQiOjIsIlRpcG9Vc3VhcmlvSWQiOjEsIlJvbGVzQ29kaWdvcyI6WyJBRE0iXSwiUGVybWlzb3NJZHMiOlsxLDIsMyw0LDUsNiw3LDgsOSwxMCwxMSwxMiwxMywxNCwxNSwxNiwxNywxOCwxOSwyMCwyMSwyMiwyMywyNCwyNSwyNiwyNywyOCwyOSwzMCwzMSwzMiwzMywzNCwzNSwzNiwzNywzOCwzOSw0MCw0MSw0Miw0Myw0NCw0NSw0Niw0Nyw0OCw0OSw1MCw1MSw1Miw1Myw1NCw1NSw1Niw1Nyw1OCw1OSw2MCw2MSw2Miw2Myw2NCw2NSw2Niw2Nyw2OCw2OSw3MCw3MSw3Miw3Myw3NCw3NSw3Niw3Nyw3OCw3OSw4MCw4MSw4Miw4Myw4NCw4NSw4Niw4Nyw4OCw4OSw5MCw5MSw5Miw5Myw5NCw5NSw5Niw5Nyw5OCw5OSwxMDAsMTAxLDEwMiwxMDMsMTA0LDEwNSwxMDYsMTA3LDEwOCwxMDksMTEwLDExMSwxMTIsMTEzLDExNCwxMTUsMTE2LDExNywxMTgsMTE5LDEyMCwxMjEsMTIyLDEyMywxMjQsMTI1LDEyNiwxMjcsMTI4LDEyOSwxMzAsMTMxLDEzMiwxMzMsMTM0LDEzNSwxMzYsMTM3LDEzOCwxMzksMTQwLDE0MSwxNDIsMTQzLDE0NCwxNDUsMTQ2LDE0NywxNDgsMTQ5LDE1MCwxNTEsMTUyLDE1MywxNTQsMTYwLDE2MSwxNjIsMTYzLDE2NCwxNjUsMTY2LDE2NywxNjgsMTY5LDE3MCwxNzEsMTcyLDE3MywxNzQsMTc1LDE3NiwxNzcsMTc4LDE3OSwxODAsMTgxLDE4MiwxODMsMTg0LDE4NSwxODYsMTg3LDE4OCwxODksMTkwLDE5MSwxOTIsMTkzLDE5NCwxOTUsMTk2LDE5NywxOTgsMTk5LDIwMCwyMDEsMjAyLDIwMywyMDQsMjA1LDIwNiwyMDcsMjA4LDIwOSwyMTAsMjExLDIxMiwyMTMsMjE0LDIxNSwyMTYsMjE3LDIxOCwyMTksMjIwLDIyMSwyMjIsMjIzLDIyNCwyMjUsMjI2LDIyNywyMzAsMjMxLDIzMiwyMzMsMjM0LDIzNSwyMzYsMjM3LDIzOCwyMzldLCJOb21icmUiOiJqYXZpdCIsIkFwZWxsaWRvIjoiYWRtaW4iLCJEbmkiOiIyNDcwMjgwNCIsIkNvZGlnb1RlbGVmb25vIjoiMTEiLCJUZWxlZm9ubyI6IjExNTM0NzU2NDQiLCJEaXJlY2Npb25DYWxsZSI6bnVsbCwiRGlyZWNjaW9uTnJvIjpudWxsLCJEaXJlY2Npb25EZXB0byI6bnVsbCwiRGlyZWNjaW9uQ29kUG9zdGFsIjpudWxsLCJMb2NhbGlkYWRJZCI6bnVsbCwiRW1haWwiOiJqYXZpdEBqYXZpdC5jb20uYXIiLCJHZW5lcm9JZCI6MSwiVGlwb0RvY3VtZW50b0lkIjoxLCJGZWNoYU5hY2ltaWVudG8iOm51bGwsIk5vbWJyZUFwbGljYWNpb24iOm51bGwsIkZvdG8iOm51bGwsIkVtcHJlc2FJZCI6MCwiRW1wcmVzYUNvZGlnbyI6IjAxIiwiVGlwb0VtcHJlc2FJZCI6NCwiVGlwb0VtcHJlc2FDb2RpZ28iOiJEUyIsIkVtcHJlc2FOb21icmUiOiJEcm9ndWVyw61hIERlbCBTdWQiLCJSZWdpc3RyYWRvQ29uUmVkU29jaWFsIjowfSwiZXhwIjoxNjI3NTY3NTg3fQ.tz1PVbs8XcnxJC9GMshHJsMy9nrHPxUNl_m-RaoRTj4"
  })
}

@Injectable({
  providedIn: 'root'
})

export class HttpService {

  constructor(private http: HttpClient, private _userService:UserService, private _router: Router, private _carrierService: CarrierService) { }

  handleError(error: HttpErrorResponse) {
    console.error(error)
  }

  login(user:User) {
    this.http.post<LoginResponse>(
      'http://localhost:16199/api/Seguridad/1/Login',
      user,
      httpOptions
    )
    .subscribe(
      res => {
        httpOptions.headers = httpOptions.headers.set('Authorization', `Bearer ${res.token}`)
        this._userService.setAsLoggedIn()
        this._router.navigateByUrl('/admin')
      },
      this.handleError
    )
  }

  getCarriers(){
    console.log(httpOptions)
    return this.http.get<GetCarriersResponse>(
      'http://localhost:16199/api/transportistas/1',
      httpOptions
    )
    .subscribe(
      carriers => this._carrierService.carriers = carriers.items,
      this.handleError
    )
  }

  addCarrier(carrier:Carrier) {
    return this.http.post<PostCarrierResponse>('http://localhost:16199/api/transportistas/1',
    carrier,
    httpOptions
    )
    .subscribe(
      carrier => this._carrierService.carriers.push(carrier.transportista),
      this.handleError
    )
  }

  deleteCarrier(id: number) {
    return this.http.delete<DeleteCarrierResponse>(`http://localhost:16199/api/transportistas/1/${id}`,
    httpOptions)
      .subscribe(
        () => {
          this._carrierService.carriers = this._carrierService.carriers.filter(c => c.id !== id)
        })
  }

  editCarrier(id: number | undefined, data: Carrier) {
    return this.http.put(`http://localhost:16199/api/transportistas/1/${id}`,
    data,
    httpOptions)
      .subscribe(
        res => {
          console.log(res)

        } 
      )
  }
}
