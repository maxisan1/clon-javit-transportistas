import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  logged = false;

  constructor() { }

  setAsLoggedIn() {
    this.logged = true
  }

  setAsLoggedOut() {
    this.logged = false
  }
}
