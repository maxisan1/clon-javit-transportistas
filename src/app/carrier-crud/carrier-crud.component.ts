import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { HttpService } from '../http.service';
import { ModalData } from '../admin/carrier';

@Component({
  selector: 'app-carrier-crud',
  templateUrl: './carrier-crud.component.html',
  styleUrls: ['./carrier-crud.component.css'],
})
export class CarrierCrudComponent implements OnInit {
  carrier: FormGroup = this._fb.group({
    nombre: [this.data.carrier?.nombre, Validators.required],
    codigo: [this.data.carrier?.codigo, Validators.required],
  });

  constructor(
    public modalRef: MatDialogRef<CarrierCrudComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ModalData,
    private _fb: FormBuilder,
    private _httpService: HttpService
  ) {}

  onClose(): void {
    this.modalRef.close();
  }

  ngOnInit(): void {}

  onSubmit() {
    console.log(this.data);
    if (this.carrier.valid) {
      if (this.data.edit) {
        //Editamos el elemento
        console.log(this.carrier.value);
        this._httpService.editCarrier(this.data.carrier.id, this.carrier.value);
        this.modalRef.close();
      } else {
        //Usamos el método create
        this._httpService.addCarrier(this.carrier.value);
        this.modalRef.close();
      }
    }
  }
}
