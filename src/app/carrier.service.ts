import { Injectable } from '@angular/core';
import { Carrier } from './admin/carrier';

@Injectable({
  providedIn: 'root'
})
export class CarrierService {

  carriers: Carrier[] = []

  constructor() { }

  getCarriers() {
    return [...this.carriers]
  }

}
