import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  loginForm: FormGroup = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required]
  })

  constructor(private router: Router, private fb: FormBuilder, private _httpService: HttpService) {  }

  ngOnInit(): void {
  }

  handleClick = () => {
    
  }

  login() {
    console.log(this.loginForm.value)
    this._httpService.login(this.loginForm.value)
    console.log()
  }

}
