export interface Carrier {
    nombre: string;
    codigo: number;
    id: number | undefined;
}

export interface GetCarriersResponse {
    items: Carrier[];
    paginado: string | null;
}

export interface PostCarrierResponse {
    transportista: Carrier;
}

export interface DeleteCarrierResponse {
    transportista: Carrier;
}

export interface ModalData {
    carrier: Carrier,
    edit: boolean
}
