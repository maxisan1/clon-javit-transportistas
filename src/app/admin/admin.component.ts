import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { CarrierService } from '../carrier.service';
import { Carrier, GetCarriersResponse } from './carrier';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { CarrierCrudComponent } from '../carrier-crud/carrier-crud.component';
import { HttpService } from '../http.service';
import { MatSidenav } from '@angular/material/sidenav';
import { Observable } from 'rxjs';
import {MatPaginator} from '@angular/material/paginator';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import { UserService } from '../user.service';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})

export class AdminComponent implements OnInit {
  @ViewChild('sidenav') sidenav!: MatSidenav;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatTable) table!: MatTable<Carrier>

  menuIsOpen = false

  carriers: Carrier[] = []

  columnsToDisplay = ['codigo', 'nombre', 'actions']
  dataSource!: MatTableDataSource<Carrier>

  constructor(
    private router:Router,
    public carrierSvc: CarrierService,
    public modal: MatDialog,
    private _httpService: HttpService,
    private _userService: UserService
  ) { }

  openModal(carrier?: Carrier): void {
    const modalRef = this.modal.open(CarrierCrudComponent, {
      width: '40%',
      data: {
        carrier,
        edit: !!carrier
      }
    })

    modalRef.afterClosed()
      .subscribe(() => this.loadCarriers())
  }

  ngOnInit(): void {
    this._httpService.getCarriers()
      .add(() => this.loadCarriers())
  }

  loadCarriers() {
    this.carriers = this.carrierSvc.getCarriers()
    this.dataSource = new MatTableDataSource(this.carriers)
    this.dataSource.paginator = this.paginator

  }

  goBack() {
    this.router.navigateByUrl('')
  }

  close() {
    this.sidenav.close()
  }

  toggleSideNav() {
    if (!this.menuIsOpen) {
      this.sidenav.open()
    } else {
      this.sidenav.close()
    }
    this.menuIsOpen = !this.menuIsOpen
  }

  logout() {
    this._userService.setAsLoggedOut()
  }

  deleteCarrier(id:number) {
    this._httpService.deleteCarrier(id)
      .add(() => this.loadCarriers())
  }
}
