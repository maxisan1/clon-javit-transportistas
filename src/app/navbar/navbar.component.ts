import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Output() openMenu = new EventEmitter()
  constructor(private _userService: UserService, private _router:Router) { }

  ngOnInit(): void {
  }

  logout() {
    this._userService.setAsLoggedOut()
    this._router.navigateByUrl('/')
  }

  open(value:any) {
    this.openMenu.emit(value)
  }



}
